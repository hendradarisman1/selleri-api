const Shop = require('../model/shopModel')

exports.addShop = async (req,res) => {
    let shop = await Shop.findOne({
        shop_name : req.body.shop_name
    })
    if(shop == null){
        let shop = new Shop({
            shop_name:req.body.shop_name            ,
            shop_description : req.body.shop_description,
            address : req.body.address,
            map_location : req.body.map_location,
            city : req.body.city,
            province : req.body.province,
            postal_code : req.body.postal_code,
            country : req.body.country,
            user_id : req.body.user_id
        })
        shop = await shop.save()
    
        if(!shop){
            return res.status(400).json({error:"Something went wrong"})
        }
        res.send(shop)
    }
    else{
        return res.status(400).json({error:"shop already exists."})
    }
}

// to view all shop
exports.listShop = async (req,res) => {
    let shop = await Shop.find()
    if(!shop){
        return res.status(400).json({error:"Something went wrong"})
    }
    res.send(shop)
}

// to find shop
exports.findShop = async(req, res) => {
    let shop = await Shop.findById(req.params.id)
    if(!shop){
        return res.status(400).json({error:"Something went wrong"})
    }
    res.send(shop)
}

// to update shop
exports.updateShop = async(req,res) => {
    let shop = await Shop.findByIdAndUpdate(req.params.id,{
       shop_name: req.body.shop_name 
    },
    {new:true})
    if(!shop){
        return res.status(400).json({error:"Something went wrong"})
    }
    res.send(shop)

}

// to delete shop
exports.deleteShop = (req, res) => {
    Shop.findByIdAndRemove(req.params.id)
    .then(shop=>{
        if(!shop){
            return res.status(400).json({error:"shop not found"})
        }
        else{
            return res.status(200).json({message: "shop deleted successfully"})
        }
    })
    .catch(err=>{return res.status(400).json({error:err.message})})
}
