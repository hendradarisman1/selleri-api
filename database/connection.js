const mongoose = require('mongoose');


mongoose.connect(process.env.MONGODB_URI, function(err) {
    if (err) {
        console.log('Unable to connect to Mongo.');
        process.exit(1);
    } else {
            console.log('Database Connected');
    }
});