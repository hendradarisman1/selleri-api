const express = require('express')
const { addShop, listShop, findShop, updateShop, deleteShop } = require('../controller/shopController')
const { requireSignin } = require('../controller/userController')
const { shopValidationSchema, validation } = require('../validation/validator')
const router = express.Router()

router.post('/addshop',shopValidationSchema, validation, requireSignin, addShop)
router.get('/shop', listShop)
router.get('/findshop/:id', findShop)
router.put('/updateshop/:id',requireSignin, updateShop)
router.delete('/deleteshop/:id',requireSignin, deleteShop)


module.exports = router