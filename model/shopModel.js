const mongoose = require('mongoose')
const {ObjectId} = mongoose.Schema
const shopSchema = mongoose.Schema({
    shop_name:{
        type:String,
        required:true,
        trim:true
    },
    shop_description:{
        type:String,
        required:true,
        trim:true
    },
    address:{
        type:String,
        required:true,
        trim:true
    },
    map_location:{
        type:String,
        required:true,
        trim:true
    },
    city:{
        type:String,
        required:true,
        trim:true
    },
    province:{
        type:String,
        required:true,
        trim:true
    },
    postal_code:{
        type:Number,
        required:true,
        trim:true
    },
    country:{
        type:String,
        required:true,
        trim:true
    },
    user_id:{
        type:String,
        required:true,
        trim:true
    }

},{
    timestamps:true})
    // createAt, updateAt

    module.exports = mongoose.model('Shop',shopSchema)